﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations
{
    class LineaEntityTypeConfiguration : 
        EntityTypeConfiguration<LineaDeProduccion>
    {
        public LineaEntityTypeConfiguration()
        {
            this.ToTable("LineasDeProduccion");
            this.HasKey<int>(linea => linea.ID);
            this.Property(linea => linea.ID).HasColumnName("IDLinea");
            this.Property(l => l.Nombre).IsRequired();
            this.HasMany<Proyecto>(linea => linea.Proyectos)
                .WithRequired(proy => proy.LineaDeProduccion)
                .HasForeignKey<int>(proy => proy.IDLinea);

        }
    }
}
