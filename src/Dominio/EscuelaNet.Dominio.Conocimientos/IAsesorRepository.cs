﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public interface IAsesorRepository : IRepository<Asesor>
    {
        Asesor Add(Asesor asesor);
        void Update(Asesor asesor);
        void Delete(Asesor asesor);
        Asesor GetAsesor(int id);
        List<Asesor> ListAsesor();
        //CONOCIMIENTO 
        Conocimiento AddConocimiento(Conocimiento conocimiento, Asesor asesor);
        void DeleteConocimiento(Conocimiento conocimiento, Asesor asesor);
    }
}
