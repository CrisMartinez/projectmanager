﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class Etapa : Entity
    {
        //public string Nombre { get => Nombre; set => Nombre = value; }
        public string Nombre { get; set; }
        //public int Duracion { get => Duracion; set => Duracion = value; }
        public int Duracion { get; set; }
        public Proyecto Proyecto { get; set; }
        public List<Tecnologias> Tecnologias { get; set; }
        public int IDProyecto { get; set; }

        //public DateTime FechaInicio { get; set; }
        //public DateTime FechaFin { get => FechaFin; set => FechaFin = value; }
        //public DateTime FechaFin { get; set; }
        private Etapa()
        {
            this.Duracion = 0;
        }

        public Etapa(string nombre)
        {
            this.Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
        }
        public Etapa(string nombre, int duracion)
        {
            this.Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            //FechaInicio = DateTime.Today;
            CambiarDuracion(duracion);

        }

        public void pushTecnologia(Tecnologias tecnologia)
        {
            if(this.Tecnologias == null)
            {
                this.Tecnologias = new List<Tecnologias>();
            }
            this.Tecnologias.Add(tecnologia);
        }

        public void pullTecnologia(Tecnologias tecnologias)
        {
            if(this.Tecnologias != null)
            {
                if (this.Tecnologias.Contains(tecnologias))
                {
                    this.Tecnologias.Remove(tecnologias);
                }
            }
        }

        public void CambiarDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Una etapa debe tener más de 0 días.");
            this.Duracion = duracion;
            //calcFechaFin();//this.FechaInicio);
        }

       /* public void calcFechaFin()//DateTime fechaini)
       {
            //Calculo de los dias habiles que tarda la etapa y se calcula FechaFin(etapa)
            int contador = 1;
     
            var fechaini = this.FechaInicio;
            
            var dia = fechaini.DayOfWeek.ToString("d");

            while (contador <= this.Duracion)
            {
                if ((dia != "5") && (dia != "6"))
                {
                    contador++;
                }
                fechaini = fechaini.AddDays(1);
                dia = fechaini.DayOfWeek.ToString("d");
            }

            this.FechaFin = fechaini;
        }*/
    }
}


