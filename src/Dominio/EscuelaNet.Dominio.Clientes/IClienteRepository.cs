﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;

namespace EscuelaNet.Dominio.Clientes
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        Cliente Add(Cliente cliente);

        void Update(Cliente cliente);

        void Delete(Cliente cliente);

        void DeleteUnidadDeNegocio(UnidadDeNegocio unidad);

        void DeleteDireccion(Direccion direccion);

        Cliente GetCliente(int id);
         
        UnidadDeNegocio GetUnidadDeNegocio(int id);
       
        Direccion GetDireccion(int id);

        List<Cliente> ListCliente();
    }

}
