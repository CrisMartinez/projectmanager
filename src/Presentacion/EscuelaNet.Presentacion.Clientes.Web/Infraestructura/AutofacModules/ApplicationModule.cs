﻿using Autofac;
using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var connectionString =
                        ConfigurationManager
                            .ConnectionStrings["ClienteContext"].ToString();
            builder.RegisterType<ClienteContext>()
                .InstancePerRequest();

            builder.Register(c => new ClientesQuery(
                            connectionString))
                        .As<IClientesQuery>()
                        .InstancePerLifetimeScope();

            builder.Register(c => new SolicitudesQuery(
                            connectionString))
                        .As<ISolicitudesQuery>()
                        .InstancePerLifetimeScope();

            builder.Register(c => new UnidadesQuery(
                            connectionString))
                        .As<IUnidadesQuery>()
                        .InstancePerLifetimeScope();

            builder.Register(c => new DireccionesQuery(
                            connectionString))
                        .As<IDireccionesQuery>()
                        .InstancePerLifetimeScope();

            builder.RegisterType<ClientesRepository>()
                        .As<IClienteRepository>()
                        .InstancePerLifetimeScope();
            
            builder.RegisterType<SolicitudesRepository>()
                        .As<ISolicitudRepository>()
                        .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}