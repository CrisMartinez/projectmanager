﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.QueryModels
{
    public class SkillQueryModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Experiencia Grados { get; set; }

    }
}
