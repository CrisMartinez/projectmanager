﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Programadores.QueryModels;

namespace EscuelaNet.Aplicacion.Programadores.QueryServices
{
    public class ProgramadorQuery : IProgramadorQuery
    {

        private string _connectionString;
        public ProgramadorQuery(string connectionString)
        {
            _connectionString = connectionString;
        }
        public ProgramadorQueryModel GetProgramador(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ProgramadorQueryModel>(
                    @"
                    SELECT p.IdProgramador as Id, p.Nombre as Nombre, p.Apellido as Apellido, p.Legajo as Legajo,
                    p.Dni as Dni, p.Rol as Rol, p.FechaNacimiento as FechaNacimiento, p.Disponibilidad as Disponibilidad
                    FROM Programador as p
                    WHERE p.IdProgramador = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<ProgramadorQueryModel> ListProgramador()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ProgramadorQueryModel>(
                    @"
                    SELECT p.IdProgramador as Id, p.Nombre as Nombre, p.Apellido as Apellido, p.Legajo as Legajo,
                    p.Dni as Dni, p.Rol as Rol, p.FechaNacimiento as FechaNacimiento, p.Disponibilidad as Disponibilidad,
                    ps.Total as Total
                    FROM Programador as p
					LEFT JOIN (
					    SELECT Programador_ID, COUNT(Programador_ID) as Total
                        FROM ProgramadorSkill 
                        GROUP BY Programador_ID
					) as ps ON p.IdProgramador = ps.Programador_ID
                    ORDER BY Total DESC
                    "
                    ).ToList();
            }
        }
    }
}
