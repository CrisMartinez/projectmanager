﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class UpdateEquipoCommandHandler : IRequestHandler<UpdateEquipoCommand, CommandRespond>
    {
        private IEquipoRepository _equipoRepositorio;
        public UpdateEquipoCommandHandler(IEquipoRepository equipoRepositorio)
        {
            _equipoRepositorio = equipoRepositorio;
        }
        public Task<CommandRespond> Handle(UpdateEquipoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    var equipo = _equipoRepositorio.GetEquipo(request.Id);
                    equipo.Nombre = request.Nombre;
                    equipo.Pais = request.Pais;
                    equipo.HusoHorario = request.HusoHorario;

                    _equipoRepositorio.Update(equipo);
                    _equipoRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Texto vacio";
                return Task.FromResult(responde);
            }
        }
    }
}
