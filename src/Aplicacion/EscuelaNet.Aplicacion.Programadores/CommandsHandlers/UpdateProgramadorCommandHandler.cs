﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class UpdateProgramadorCommandHandler : IRequestHandler<UpdateProgramadorCommand, CommandRespond>
    {
        private IProgramadorRepository _programadorRepositorio;
        public UpdateProgramadorCommandHandler(IProgramadorRepository programadorRepositorio)
        {
            _programadorRepositorio = programadorRepositorio;
        }
        public Task<CommandRespond> Handle(UpdateProgramadorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    var programador = _programadorRepositorio.GetProgramador(request.Id);
                    programador.Nombre = request.Nombre;
                    programador.Apellido = request.Apellido;
                    programador.Legajo = request.Legajo;
                    programador.Dni = request.Dni;
                    programador.Rol = request.Rol;
                    programador.FechaNacimiento = request.FechaNacimiento;
                    programador.Disponibilidad = request.Disponibilidad;

                    _programadorRepositorio.Update(programador);
                    _programadorRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Texto vacio";
                return Task.FromResult(responde);
            }
        }
    }
}
