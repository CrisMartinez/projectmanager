﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class DeleteSkillCommandHandler : IRequestHandler<DeleteSkillCommand, CommandRespond>
    {
        private ISkillRepository _skillRepositorio;
        public DeleteSkillCommandHandler(ISkillRepository skillRepositorio)
        {
            _skillRepositorio = skillRepositorio;
        }
        public Task<CommandRespond> Handle(DeleteSkillCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var skill = _skillRepositorio.GetSkill(request.Id);
                _skillRepositorio.Delete(skill);
                _skillRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);

            }


        }
    }
}
