﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;


namespace EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand
{
    public class NuevoClienteCommand : IRequest<CommandRespond>
    {
        public string RazonSocial { get; set; }

        public string Email { get; set; }

        public Categoria Categoria { get; set; }

    }
}
