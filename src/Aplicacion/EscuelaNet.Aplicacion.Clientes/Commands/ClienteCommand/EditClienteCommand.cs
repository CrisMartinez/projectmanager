﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand
{
    public class EditClienteCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string RazonSocial { get; set; }
        public string Email { get; set; }
        public Categoria Categoria { get; set; }
    }
}
