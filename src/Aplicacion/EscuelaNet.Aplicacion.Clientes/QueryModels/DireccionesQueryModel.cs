﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryModels
{
    public class DireccionesQueryModel
    {
        public int ID { get; set; }

        public string Domicilio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }

        public int IdUnidadDeNegocio { get; set; }

        public int IdCliente { get; set; }

        public string RazonSocialUnidad { get; set; }
    }
}
