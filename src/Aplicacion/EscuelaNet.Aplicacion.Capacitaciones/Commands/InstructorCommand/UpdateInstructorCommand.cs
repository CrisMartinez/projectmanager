﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand
{
    public class UpdateInstructorCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public IList<Tema> Temas { get; set; }

        [DisplayName("Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }
    }
}
